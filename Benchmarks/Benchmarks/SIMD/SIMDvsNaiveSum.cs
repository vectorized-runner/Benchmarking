﻿using System;
using System.Numerics;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Jobs;

namespace Benchmarks
{
	[ShortRunJob(RuntimeMoniker.Net60, Jit.RyuJit, Platform.AnyCpu)]
	[ShortRunJob(RuntimeMoniker.NetCoreApp31, Jit.RyuJit, Platform.AnyCpu)]
	public class SIMDvsNaiveSum
	{
		static int ArrayLength = 16 * 100_000;
		static int[] Rhs = new int[ArrayLength];
		static int[] Lhs = new int[ArrayLength];
		static int[] Result = new int[ArrayLength];
		Random Random = new();

		[IterationSetup]
		public void Setup()
		{
			for(int i = 0; i < ArrayLength; i++)
			{
				Lhs[i] = Random.Next();
				Rhs[i] = Random.Next();
			}
		}

		[Benchmark(Baseline = true)]
		public void BenchmarkSIMDArrayAddition()
		{
			SIMDArrayAddition(Rhs, Lhs, Result);
		}

		[Benchmark]
		public void BenchmarkNaiveArrayAddition()
		{
			NaiveArrayAddition(Rhs, Lhs, Result);
		}

		[Benchmark]
		public void BenchmarkUnrolledArrayAddition()
		{
			UnrolledArrayAddition(Rhs, Lhs, Result);
		}

		public static void SIMDArrayAddition(int[] lhs, int[] rhs, int[] result)
		{
			var simdLength = Vector<int>.Count;
			int i;
			for(i = 0; i <= lhs.Length - simdLength; i += simdLength)
			{
				var va = new Vector<int>(lhs, i);
				var vb = new Vector<int>(rhs, i);
				(va + vb).CopyTo(result, i);
			}

			// Input array length, may not be multiple of Vector<T>, this sums any of the remaining elements
			for(; i < lhs.Length; ++i)
			{
				result[i] = lhs[i] + rhs[i];
			}
		}

		public static void NaiveArrayAddition(int[] lhs, int[] rhs, int[] result)
		{
			for(int i = 0; i < result.Length; i++)
			{
				result[i] = lhs[i] + rhs[i];
			}
		}
		
		public static void UnrolledArrayAddition(int[] lhs, int[] rhs, int[] result)
		{
			for(int i = 0; i < result.Length; i += 4)
			{
				result[i] = lhs[i] + rhs[i];
				result[i + 1] = lhs[i + 1] + rhs[i + 1];
				result[i + 2] = lhs[i + 2] + rhs[i + 2];
				result[i + 3] = lhs[i + 3] + rhs[i + 3];
			}
		}
	}
}