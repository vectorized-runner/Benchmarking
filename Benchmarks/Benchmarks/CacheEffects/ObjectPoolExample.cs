﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	[ShortRunJob]
	[HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions, HardwareCounter.TotalCycles, HardwareCounter.BranchInstructions, HardwareCounter.Timer)]
	public class ObjectPoolExample
	{
		public class Pool_Queue<T> where T : new()
		{
			public Queue<T> Items = new Queue<T>();

			public void Add(T item)
			{
				Items.Enqueue(item);
			}

			public T Get()
			{
				return Items.Count == 0 ? new T() : Items.Dequeue();
			}
		}

		public class Pool_Stack<T> where T : new()
		{
			public Stack<T> Items = new Stack<T>();

			public void Add(T item)
			{
				Items.Push(item);
			}

			public T Get()
			{
				return Items.Count == 0 ? new T() : Items.Pop();
			}
		}

		public class MyObject
		{
			public int Value1;
			public int Value2;
			public int Value3;
			public int Value4;

			public int Sum() => Value1 + Value2 + Value3 + Value4;
		}

		Pool_Queue<MyObject> QueuePool = new();
		Pool_Stack<MyObject> StackPool = new();
		const int Count = 1_000_000;

		public ObjectPoolExample()
		{
			var random = new Random();

			// 1000 object pre-pooled (like real life example)
			for(int i = 0; i < 1000; i++)
			{
				var a = random.Next();
				var b = random.Next();
				var c = random.Next();
				var d = random.Next();
				var q = new MyObject
				{
					Value1 = a,
					Value2 = b,
					Value3 = c,
					Value4 = d,
				};
				QueuePool.Add(q);
			}

			for(int i = 0; i < 1000; i++)
			{
				var a = random.Next();
				var b = random.Next();
				var c = random.Next();
				var d = random.Next();
				var s = new MyObject
				{
					Value1 = a,
					Value2 = b,
					Value3 = c,
					Value4 = d,
				};
				StackPool.Add(s);
			}
		}

		[Benchmark]
		public int QueuePoolSum()
		{
			unchecked
			{
				var sum = 0;
				for(int i = 0; i < Count; i++)
				{
					var myObject = QueuePool.Get();

					// Doing some operations on the object
					myObject.Value1 = 0;
					myObject.Value3 = -1;

					sum += myObject.Sum();
					QueuePool.Add(myObject);
				}

				return sum;
			}
		}

		[Benchmark]
		public int NoPoolSum()
		{
			unchecked
			{
				var sum = 0;
				for(int i = 0; i < Count; i++)
				{
					var myObject = new MyObject();

					// Doing some operations on the object
					myObject.Value1 = 0;
					myObject.Value3 = -1;

					sum += myObject.Sum();
				}

				return sum;
			}
		}

		[Benchmark]
		public int StackPoolSum()
		{
			unchecked
			{
				var sum = 0;
				for(int i = 0; i < Count; i++)
				{
					var myObject = StackPool.Get();

					// Doing some operations on the object
					myObject.Value1 = 0;
					myObject.Value3 = -1;

					sum += myObject.Sum();
					StackPool.Add(myObject);
				}

				return sum;
			}
		}
	}
}