﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	[ShortRunJob]
	[HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions)]
	public class ClassVsStructIteration
	{
		public struct EntityStruct
		{
			public float Value;
		}

		public class EntityClass
		{
			public float Value;
		}

		public class DummyObject
		{
			public int[] Values = new int[100];
		}

		const int Count = 100_000;
		EntityStruct[] Structs = new EntityStruct[Count];
		EntityClass[] Classes = new EntityClass[Count];

		List<DummyObject> DummyObjectCache = new();

		public ClassVsStructIteration()
		{
			var random = new Random(0);
			for(int i = 0; i < Count; i++)
			{
				DummyObjectCache.Add(new DummyObject());
				Structs[i] = new EntityStruct { Value = (float)random.NextDouble() };
				DummyObjectCache.Add(new DummyObject());
				Classes[i] = new EntityClass { Value = (float)random.NextDouble() };
			}
		}

		[Benchmark]
		public float SumStruct()
		{
			var sum = 0f;
			for(int i = 0; i < Count; i++)
			{
				sum += Structs[i].Value;
			}
			return sum;
		}

		[Benchmark]
		public float SumClass()
		{
			var sum = 0f;
			for(int i = 0; i < Count; i++)
			{
				sum += Classes[i].Value;
			}
			return sum;
		}

	}
}