﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	[ShortRunJob]
	// [HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions)]
	public class MemorySizeCacheEffect
	{
		const int Count = 100_000_000;
		double[] Doubles = new double[Count];
		float[] Floats = new float[Count];
		long[] Longs = new long[Count];
		int[] Ints = new int[Count];
		short[] Shorts = new short[Count];
		char[] Chars = new char[Count];
		byte[] Bytes = new byte[Count];

		public MemorySizeCacheEffect()
		{
			var random = new Random(0);
			for(int i = 0; i < Count; i++)
			{
				Ints[i] = random.Next();
				Shorts[i] = (short)random.Next(short.MaxValue);
				Chars[i] = (char)random.Next(char.MaxValue);
				Bytes[i] = (byte)random.Next(byte.MaxValue);
				Longs[i] = random.Next();
				Floats[i] = (float)random.NextDouble();
				Doubles[i] = random.NextDouble();
			}
		}

		[Benchmark(Baseline = true)]
		public int SumInts()
		{
			unchecked
			{
				var sum = 0;
				for(int i = 0; i < Count; i++)
				{
					sum += Ints[i];
				}

				return sum;
			}
		}

		[Benchmark]
		public short SumShorts()
		{
			unchecked
			{
				short sum = 0;
				for(int i = 0; i < Count; i++)
				{
					sum += Shorts[i];
				}

				return sum;
			}
		}

		[Benchmark]
		public char SumChars()
		{
			unchecked
			{
				char sum = (char)0;
				for(int i = 0; i < Count; i++)
				{
					sum += Chars[i];
				}

				return sum;
			}
		}

		[Benchmark]
		public byte SumBytes()
		{
			unchecked
			{
				byte sum = 0;
				for(int i = 0; i < Count; i++)
				{
					sum += Bytes[i];
				}

				return sum;
			}
		}

		[Benchmark]
		public long SumLongs()
		{
			unchecked
			{
				long sum = 0;
				for(int i = 0; i < Count; i++)
				{
					sum += Longs[i];
				}

				return sum;
			}
		}

		[Benchmark]
		public float SumFloats()
		{
			unchecked
			{
				float sum = 0;
				for(int i = 0; i < Count; i++)
				{
					sum += Floats[i];
				}

				return sum;
			}
		}

		[Benchmark]
		public double SumDoubles()
		{
			unchecked
			{
				double sum = 0;
				for(int i = 0; i < Count; i++)
				{
					sum += Doubles[i];
				}

				return sum;
			}
		}
	}
}