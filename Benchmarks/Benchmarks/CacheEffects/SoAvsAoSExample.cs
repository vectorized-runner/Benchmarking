﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	// This had a lot less effect that I expected
	[ShortRunJob]
	[HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions)]
	public class SoAvsAoSExample
	{
		const int Count = 100_000;

		public struct Position
		{
			public float x;
			public float y;

			public float DistanceSq(Position other)
			{
				return (x - other.x) * (x - other.x) + (y - other.y) * (y - other.y);
			}
		}
		
		public struct SoAData
		{
			public Position[] Positions;
			public int[] Dummy1s;
			public int[] Dummy2s;
			public int[] Dummy3s;
			public int[] Dummy4s;
			public int[] Damages;
		}

		public struct Entity
		{
			public Position Position;
			// Dummy data that trashes the cache
			// 64 bit, so cache line is already lost after these 2 ints
			public int Dummy1;
			public int Dummy2;
			public int Damage;
		}

		SoAData SoAEntities = new SoAData
		{
			Positions = new Position[Count],
			Damages = new int[Count],
			Dummy1s = new int[Count], 
			Dummy2s = new int[Count], 
			Dummy3s = new int[Count], 
			Dummy4s = new int[Count], 
		};
		Entity[] AoSEntities = new Entity[Count];

		public SoAvsAoSExample()
		{
			var random = new Random(0);
			
			for(int i = 0; i < Count; i++)
			{
				var x = (float)random.NextDouble();
				var y = (float)random.NextDouble();
				var position = new Position
				{
					x = x,
					y = y,
				};
				var damage = random.Next();

				SoAEntities.Damages[i] = damage;
				SoAEntities.Positions[i] = position;
				AoSEntities[i].Position = position;
				AoSEntities[i].Damage = damage;
			}
		}

		[Benchmark]
		public float SoASimulation()
		{
			unchecked
			{
				var damage = 0f;

				for(int i = 0; i < Count; i++)
				{
					damage += SoAEntities.Damages[i] * SoAEntities.Positions[i].DistanceSq(new Position());
				}

				return damage;
			}
		}

		[Benchmark]
		public float AoSSimulation()
		{
			unchecked
			{
				var damage = 0f;

				for(int i = 0; i < Count; i++)
				{
					damage += AoSEntities[i].Damage * AoSEntities[i].Position.DistanceSq(new Position());
				}

				return damage;
			}
		}
	}
}