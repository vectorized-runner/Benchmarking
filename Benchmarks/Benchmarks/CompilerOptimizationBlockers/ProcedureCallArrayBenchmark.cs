﻿using System;
using System.Runtime.CompilerServices;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks.CompilerOptimizationBlockers
{
    //[HardwareCounters(HardwareCounter.BranchMispredictions, HardwareCounter.InstructionRetired, HardwareCounter.CacheMisses)]
    [ShortRunJob]
    public class ProcedureCallArrayBenchmark
    {
        private int Count = 10_000_000;
        private int[] Array;

        public ProcedureCallArrayBenchmark()
        {
            Array = new int[Count];
            var random = new Random(0);

            for (int i = 0; i < Count; i++)
            {
                Array[i] = random.Next(0, 100);
            }
        }

        [Benchmark]
        public int SumArrayLengthNotCached()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < Array.Length; i++)
                {
                    sum += Array[i];
                }

                return sum;
            }
        }
        
        [Benchmark]
        public int SumArrayLengthPointer()
        {
            unchecked
            {
                unsafe
                {
                    var sum = 0;
                    var length = Array.Length;
                    
                    fixed (int* ptr = Array)
                    {
                        for (int i = 0; i < length; i++)
                        {
                            sum += ptr[i];
                        }

                    }
                    
                    return sum;
                }
            }
        }

        [Benchmark(Baseline = true)]
        public int SumArrayLengthCached()
        {
            unchecked
            {
                var sum = 0;
                var count = Array.Length;
                for (int i = 0; i < count; i++)
                {
                    sum += Array[i];
                }

                return sum;
            }
        }
        
        [Benchmark]
        public int SumArrayLengthBehindProcedure()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < GetLength(Array); i++)
                {
                    sum += Array[i];
                }

                return sum;
            }
        }
        
        [Benchmark]
        public int SumArrayLengthBehindProcedureInlined()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < GetLengthInlined(Array); i++)
                {
                    sum += Array[i];
                }

                return sum;
            }
        }
        
        [Benchmark]
        public int SumArrayLengthBehindProcedureStatic()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < GetLengthStatic(Array); i++)
                {
                    sum += Array[i];
                }

                return sum;
            }
        }

        private int PropLength => Array.Length;
        
        [Benchmark]
        public int SumArrayLengthProperty()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < PropLength; i++)
                {
                    sum += Array[i];
                }

                return sum;
            }
        }
        
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetLengthInlined(int[] array)
        {
            return array.Length;
        }

        private int GetLength(int[] array)
        {
            return array.Length;
        }
        
        private static int GetLengthStatic(int[] array)
        {
            return array.Length;
        }
        
        
    }
}