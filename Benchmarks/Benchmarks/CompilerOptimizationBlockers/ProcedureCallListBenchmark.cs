﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks.CompilerOptimizationBlockers
{
    //[HardwareCounters(HardwareCounter.BranchMispredictions, HardwareCounter.InstructionRetired, HardwareCounter.CacheMisses)]
    [ShortRunJob]
    public class ProcedureCallListBenchmark
    {
        private int Count = 10_000_000;
        private List<int> List;

        public ProcedureCallListBenchmark()
        {
            List = new List<int>(Count);
            var random = new Random(0);

            for (int i = 0; i < Count; i++)
            {
                List.Add(random.Next(0, 100));
            }
        }

        [Benchmark]
        public int SumListLengthNotCached()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < List.Count; i++)
                {
                    sum += List[i];
                }

                return sum;
            }
        }
        

        [Benchmark(Baseline = true)]
        public int SumListLengthCached()
        {
            unchecked
            {
                var sum = 0;
                var count = List.Count;
                for (int i = 0; i < count; i++)
                {
                    sum += List[i];
                }

                return sum;
            }
        }
        
        [Benchmark]
        public int SumListLengthBehindProcedure()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < GetLength(List); i++)
                {
                    sum += List[i];
                }

                return sum;
            }
        }
        
        [Benchmark]
        public int SumListLengthBehindProcedureInlined()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < GetLengthInlined(List); i++)
                {
                    sum += List[i];
                }

                return sum;
            }
        }
        
        [Benchmark]
        public int SumListLengthBehindProcedureStatic()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < GetLengthStatic(List); i++)
                {
                    sum += List[i];
                }

                return sum;
            }
        }

        private int PropLength => List.Count;
        
        [Benchmark]
        public int SumListLengthProperty()
        {
            unchecked
            {
                var sum = 0;
                for (int i = 0; i < PropLength; i++)
                {
                    sum += List[i];
                }

                return sum;
            }
        }
        
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetLengthInlined(List<int> List)
        {
            return List.Count;
        }

        private int GetLength(List<int> List)
        {
            return List.Count;
        }
        
        private static int GetLengthStatic(List<int> List)
        {
            return List.Count;
        }
        
        
    }
}