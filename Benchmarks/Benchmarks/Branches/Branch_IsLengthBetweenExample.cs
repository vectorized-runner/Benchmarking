﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	[HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions)]
	public class Branch_IsLengthBetweenExample
	{
		public struct float3
		{
			public float x;
			public float y;
			public float z;
			
			public float LengthSq() => x * x + y * y + z * z;

			public bool IsLengthBetween_Branch(float min, float max)
			{
				var lengthSq = LengthSq();
				return lengthSq >= min && lengthSq <= max;
			}

			public bool IsLengthBetween_Branchless(float min, float max)
			{
				var lengthSq = LengthSq();
				return lengthSq >= min & lengthSq <= max;
			}
			
			public bool IsLengthBetween_BitshiftHack(float min, float max)
			{
				var lengthSq = LengthSq();
				var a = (int)(lengthSq - min) >> 31;
				var b = (int)(max - lengthSq) >> 31;
				return (a | b) == 0;
			}
		}

		const int Count = 100_000;
		float3[] Floats = new float3[Count];

		float MinValue = 0.5f;
		float MaxValue = 1.5f;

		public Branch_IsLengthBetweenExample()
		{
			var random = new Random(0);
			for(int i = 0; i < Count; i++)
			{
				Floats[i] = new float3
				{
					x = (float)random.NextDouble(),
					y = (float)random.NextDouble(),
					z = (float)random.NextDouble()
				};
			}
		}

		[Benchmark]
		public void RunBranch()
		{
			for(int i = 0; i < Count; i++)
			{
				Floats[i].IsLengthBetween_Branch(MinValue, MaxValue);
			}
		}

		[Benchmark]
		public void RunBranchless()
		{
			for(int i = 0; i < Count; i++)
			{
				var value = Floats[i].IsLengthBetween_Branchless(MinValue, MaxValue);
			}
		}

		[Benchmark]
		public void RunBitshiftHack()
		{
			for(int i = 0; i < Count; i++)
			{
				var value = Floats[i].IsLengthBetween_BitshiftHack(MinValue, MaxValue);
			}
		}
	}
}