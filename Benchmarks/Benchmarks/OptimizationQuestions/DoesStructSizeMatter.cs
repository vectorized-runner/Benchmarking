﻿using System;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	[ShortRunJob]
	public class DoesStructSizeMatter
	{
		public struct StructWithCompactData
		{
			public double UsedValue;
		}
		
		public struct StructWithUnusedData
		{
			public double UsedValue;
			public double Unused1;
			public double Unused2;
			public double Unused3;
			public double Unused4;
			public double Unused5;
			public double Unused6;
			public double Unused7;
		}
		
		const int Count = 100_000;
		StructWithCompactData[] Compact;
		StructWithUnusedData[] Unused;

		public DoesStructSizeMatter()
		{
			var random = new Random(0);
			Compact = new StructWithCompactData[Count];
			Unused = new StructWithUnusedData[Count];

			for(int i = 0; i < Count; i++)
			{
				var value = (float)random.NextDouble();
				Compact[i] = new StructWithCompactData { UsedValue = value };
				Unused[i] = new StructWithUnusedData { UsedValue = value };
			}
		}
		
		[Benchmark]
		public double DoSimpleOperationCompactStruct()
		{
			var sum = 0d;
			for(int i = 0; i < Count; i++)
			{
				sum += Compact[i].UsedValue;
			}

			return sum;
		}
		
		[Benchmark]
		public double DoSimpleOperationUnusedStruct()
		{
			var sum = 0d;
			for(int i = 0; i < Count; i++)
			{
				sum += Unused[i].UsedValue;
			}

			return sum;
		}

		[Benchmark(Baseline = true)]
		public double DoHeavyOperationCompactStruct()
		{
			var sum = 0d;
			for(int i = 0; i < Count; i++)
			{
				// Simulating a random, unpredictable branch here
				if(Compact[i].UsedValue > 0.5f)
				{
					// Simulating heavy math operation here
					sum += Math.Abs(Math.Sin(Math.Sqrt(Math.Clamp(Compact[i].UsedValue, -1, 1))));
				}
			}

			return sum;
		}

		[Benchmark]
		public double DoHeavyOperationUnusedStruct()
		{
			var sum = 0d;
			for(int i = 0; i < Count; i++)
			{
				// Simulating a random, unpredictable branch here
				if(Unused[i].UsedValue > 0.5f)
				{
					// Simulating heavy math operation here
					sum += Math.Abs(Math.Sin(Math.Sqrt(Math.Clamp(Unused[i].UsedValue, -1, 1))));
				}
			}

			return sum;
		}
	}
}