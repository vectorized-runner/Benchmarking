﻿using System;
using System.Numerics;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	[ShortRunJob]
	public class IsSoAFasterThanAoS
	{
		public struct SoACharacterData
		{
			public double[] Scores;
			public bool[] Alives;
			// Totally unused
			public Matrix4x4[] Matrices;
		}

		public struct CharacterData
		{
			public double Score;
			public bool Alive;
			// Totally Unused
			public Matrix4x4 Matrix;
		}

		const int Count = 100_000;

		SoACharacterData SoA;
		CharacterData[] AoS;
		
		public IsSoAFasterThanAoS()
		{
			SoA = new SoACharacterData
			{
				Alives = new bool[Count],
				Scores = new double[Count],
				Matrices = new Matrix4x4[Count],
			};

			AoS = new CharacterData[Count];

			var random = new Random(0);

			for(int i = 0; i < Count; i++)
			{
				var isAlive = random.NextDouble() > 0.5f;
				var score = random.NextDouble();

				SoA.Alives[i] = isAlive;
				SoA.Scores[i] = score;

				AoS[i].Alive = isAlive;
				AoS[i].Score = score;
			}
		}

		[Benchmark(Baseline = true)]
		public double AoSSimpleComputation()
		{
			var result = 0d;
			for(int i = 0; i < Count; i++)
			{
				result += AoS[i].Score;
			}

			return result;
		}
		
		[Benchmark]
		public double SoASimpleComputation()
		{
			var result = 0d;
			for(int i = 0; i < Count; i++)
			{
				result += SoA.Scores[i];
			}

			return result;
		}
		
		[Benchmark]
		public double AoSHardComputation()
		{
			var result = 0d;
			for(int i = 0; i < Count; i++)
			{
				if(AoS[i].Alive)
				{
					result += Math.Sqrt(AoS[i].Score);
				}
			}

			return result;
		}
		
		[Benchmark]
		public double SoAHardComputation()
		{
			var result = 0d;
			for(int i = 0; i < Count; i++)
			{
				if(SoA.Alives[i])
				{
					result += Math.Sqrt(SoA.Scores[i]);
				}
			}

			return result;
		}
	}
	
}