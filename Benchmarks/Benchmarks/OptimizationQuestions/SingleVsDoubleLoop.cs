﻿using System;
using BenchmarkDotNet.Attributes;

namespace Benchmarks
{
	public class SingleVsDoubleLoop
	{
		const int Count = 1_000;

		[Benchmark(Baseline = true)]
		public double SingleLoop()
		{
			var resultAs = 0d;
			var resultBs = 0d;

			for(int i = 1; i <= Count; i++)
			{
				for(int j = 1; j <= Count; j++)
				{
					resultAs += Math.Sqrt((float)i / j);
					resultBs += Math.Sin((float)i / j);
				}
			}

			return resultAs + resultBs;
		}
		
		[Benchmark]
		public double DoubleLoop()
		{
			var resultAs = 0d;

			for(int i = 1; i <= Count; i++)
			{
				for(int j = 1; j <= Count; j++)
				{
					resultAs += Math.Sqrt((float)i / j);
				}
			}
			
			var resultBs = 0d;
			
			for(int i = 1; i <= Count; i++)
			{
				for(int j = 1; j <= Count; j++)
				{
					resultBs += Math.Sin((float)i / j);
				}
			}

			return resultAs + resultBs;
		}
	}
}