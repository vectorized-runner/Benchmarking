﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnosers;

namespace Benchmarks
{
	[ShortRunJob]
	[HardwareCounters(HardwareCounter.CacheMisses, HardwareCounter.InstructionRetired, HardwareCounter.BranchMispredictions)]
	public class VirtualMethodsVsSortingByType
	{
		public abstract class Shape
		{
			public abstract double GetArea();
		}

		public class CircleC : Shape
		{
			public double Radius;
			
			public override double GetArea()
			{
				return Math.PI * Radius * Radius;
			}
		}

		public class SquareC : Shape
		{
			public double EdgeA;
			public double EdgeB;
			
			public override double GetArea()
			{
				return EdgeA * EdgeB;
			}
		}

		public struct CircleS
		{
			public double Radius;
			
			public double GetArea()
			{
				return Math.PI * Radius * Radius;
			}
		}

		public struct SquareS
		{
			public double EdgeA;
			public double EdgeB;
			
			public double GetArea()
			{
				return EdgeA * EdgeB;
			}
		}

		const int Count = 100_000;

		List<Shape> Shapes = new(Count);
		List<CircleS> Circles = new(Count);
		List<SquareS> Squares = new(Count);
		
		public VirtualMethodsVsSortingByType()
		{
			var random = new Random(0);
			for(int i = 0; i < Count; i++)
			{
				var square = random.NextDouble() > 0.5f;
				if(square)
				{
					var a = random.NextDouble();
					var b = random.NextDouble();
					Shapes.Add(new SquareC
					{
						EdgeA = a,
						EdgeB = b,
					});
					Squares.Add(new SquareS
					{
						EdgeA = a,
						EdgeB = b,
					});
				}
				else
				{
					var r = random.NextDouble();
					Shapes.Add(new CircleC
					{
						Radius = r,
					});
					Circles.Add(new CircleS
					{
						Radius = r,
					});
				}
			}
		}

		[Benchmark]
		public double SumAllAreasForClass()
		{
			var totalArea = 0d;
			for(int i = 0; i < Count; i++)
			{
				totalArea += Shapes[i].GetArea();
			}

			return totalArea;
		}

		[Benchmark]
		public double SumAllAreasForStruct()
		{
			var totalArea = 0d;
			for(int i = 0; i < Circles.Count; i++)
			{
				totalArea += Circles[i].GetArea();
			}
			for(int i = 0; i < Squares.Count; i++)
			{
				totalArea += Squares[i].GetArea();
			}

			return totalArea;
		}
	}
}